import React, { Component } from 'react'
import TodoTaskService from '../services/TodoTaskService';

class CreateTaskComponent extends Component {

constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            task: '',
            status: ''
        }
        this.changeTaskNameHandler = this.changeTaskNameHandler.bind(this);
        this.changeStatusHandler = this.changeStatusHandler.bind(this);
         this.saveOrUpdateTask = this.saveOrUpdateTask.bind(this);
    }

 // step 3
    componentDidMount(){
     console.log ('Hello');
        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            TodoTaskService.getTaskById(this.state.id).then( (res) =>{
                let task = res.data;
                this.setState({taskName: task.task,
                    taskStatus: task.status
                });
            });
        }
    }

    saveOrUpdateTask = (e) => {
            e.preventDefault();
            let task = {task: this.state.taskName, status: this.state.taskStatus};
            console.log('tasks => ' + JSON.stringify(task));

            // step 5
            if(this.state.id === '_add'){
                TodoTaskService.createTask(task).then(res =>{
                    this.props.history.push('/tasks');
                });
            }else{
                TodoTaskService.updateTask(task, this.state.id).then( res => {
                    this.props.history.push('/tasks');
                });
            }
        }

    changeTaskNameHandler= (event) => {
        this.setState({taskName: event.target.value});
    }

    changeStatusHandler= (event) => {
        this.setState({taskStatus: event.target.value});
    }

    cancel(){
        this.props.history.push('/tasks');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Task</h3>
        }else{
            return <h3 className="text-center">Update Task</h3>
        }
    }

   render() {
           return (
               <div>
                   <br></br>
                      <div className = "container">
                           <div className = "row">
                               <div className = "card col-md-6 offset-md-3 offset-md-3">
                                   {
                                       this.getTitle()
                                   }
                                   <div className = "card-body">
                                       <form>
                                           <div className = "form-group">
                                               <label> Task Name: </label>
                                               <input placeholder="Task Name" name="taskName" className="form-control"
                                                   value={this.state.taskName} onChange={this.changeTaskNameHandler}/>
                                           </div>
                                           <div className = "form-group">
                                               <label> Status: </label>
                                               <input placeholder="Status" name="status" className="form-control"
                                                   value={this.state.taskStatus} onChange={this.changeStatusHandler}/>
                                           </div>


                                           <button className="btn btn-success" onClick={this.saveOrUpdateTask}>Save</button>
                                           <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                       </form>
                                   </div>
                               </div>
                           </div>

                      </div>
               </div>
           )
       }

}

export default CreateTaskComponent