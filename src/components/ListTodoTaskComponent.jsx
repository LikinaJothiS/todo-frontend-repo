import React, { Component } from 'react'
import TodoTaskService from '../services/TodoTaskService'

class ListTodoTaskComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                tasks: []
        }
        this.addTask = this.addTask.bind(this);
        this.editTask = this.editTask.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
    }



    componentDidMount(){
        TodoTaskService.getTasks().then((res) => {
            this.setState({ tasks: res.data});
        });
    }

    addTask(){
        this.props.history.push('/add-task/_add');
    }

    deleteTask(id){
        TodoTaskService.deleteTask(id).then( res => {
            this.setState({tasks: this.state.tasks.filter(task => task.id !== id)});
        });
    }

    editTask(id){
        this.props.history.push(`/add-task/${id}`);
    }

    render() {
        return (
            <div>
                 <h2 className="text-center">Todo Task List</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addTask}> Add Task</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Task Name</th>
                                    <th> Status </th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.tasks.map(
                                        task =>
                                        <tr key = {task.id}>
                                             <td> { task.task} </td>
                                             <td> {task.status}</td>

                                        <td>
                                             <button  onClick={ () => this.editTask(task.id)} className="btn btn-info">Update </button>
                                             <button style={{marginLeft: "10px"}}  onClick={ () => this.deleteTask(task.id)} className="btn btn-danger">Delete </button>
                                             <button style={{marginLeft: "10px"}}  className="btn btn-info">View </button>
                                         </td>
                                        </tr>

                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListTodoTaskComponent
