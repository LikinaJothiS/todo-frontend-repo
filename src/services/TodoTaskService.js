import axios from 'axios';

//const TODOS_API_BASE_URL = "/api/v1/todo";
const TODOS_API_BASE_URL = "http://localhost:8080/api/v1/todo";


class TodoTaskService {

    getTasks(){
        return axios.get(TODOS_API_BASE_URL + '/');
    }

    getTaskById(taskId){
            return axios.get(TODOS_API_BASE_URL + '/' + taskId);
    }

     createTask(task){
        return axios.post(TODOS_API_BASE_URL + '/new' , task);
    }

    updateTask(task, taskId){
            return axios.put(TODOS_API_BASE_URL + '/' + taskId, task);
    }

    deleteTask(taskId){
            return axios.delete(TODOS_API_BASE_URL + '/' + taskId);
        }
}

export default new TodoTaskService()